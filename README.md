Minhaty
===

## Table of Contents

[TOC]


---

:dart: Objective
---

 
*  The national portal for requests for scholarships for higher education and vocational training, which would simplify procedures for students wishing to benefit from the higher education grant.
*  A connection space offered for all types of student.
*  Moroccan students with national or foreign baccalaureate degrees can register on the site.

:memo: Environment and development tools
--

* **Laravel** is a free, open-source PHP web framework, created by Taylor Otwell and intended for the development of web applications following the model–view–controller (MVC) architectural pattern and based on Symfony. Some of the features of Laravel are a modular packaging system with a dedicated dependency manager, different ways for accessing relational databases, utilities that aid in application deployment and maintenance, and its orientation toward syntactic sugar.
* **Vue.js**  is an open-source Model–view–viewmodel JavaScript framework for building user interfaces and single-page applications. It was created by Evan You, and is maintained by him and the rest of the active core team members coming from various companies such as Netlify and Netguru.
* **Bulma** is a free, open source CSS framework based on Flexbox.
* **Laravel Enso** is a starting project, based on Laravel 5.5, Vue 2, Bulma, and integrated themes from Bulmaswatch, utilizing Vueex and VueRouter.

Installation Steps 'Laravel Enso'
---
* Download the project with git clone https://github.com/laravel-enso/enso.git --depth 1

* Within the project folder run composer install

* Create a database for your site (see the Laravel database documentation), copy or rename the .env.example file to .env, edit the database configuration information, and run php artisan key:generate

* In order to serve the back-end API, take a look at the Local Development Server section of the Laravel installation documentation and consider using Valet for a better experience

* Run php artisan migrate --seed

* Open the client folder, copy the .env.example file, save it as .env and set the URL for the back-end API (which you've configured at step 4)

* Run yarn && yarn build

* Launch the site and log into the project with user: admin@laravel-enso.com, password: password

* For live reload / hot module replacement functionality run yarn serve

* (optional) Setup the configuration files as needed, in config/enso/*.php

**Enjoy!**






:camera: Screenshots
---

* Add New Student

![](https://i.imgur.com/R73B7cf.jpg)

* Change student information.

![](https://i.imgur.com/lJwFPIn.jpg)

* List of students

![](https://i.imgur.com/nl3x9LT.jpg)

Video
---

[![Watch the video](https://i.imgur.com/wkM14uK.png)](https://www.youtube.com/watch?v=XIMDmcXUg8s&feature=youtu.be)



Thanks
---
A special thank  to **Professor [Baddi Youssef](https://www.linkedin.com/in/youssefbaddi/)** for all the support he provided us and for giving us an opportunity to learn new technologies.

Created By
---
 * **Ahammad Abdellatif.**
 * **Lafryhi Yassine.**
 * **Mahmoud Zakaria.**


###### tags: `Minhaty` 

