const StudentIndex = () => import('../../../../pages/school/member/students/Index.vue');

export default {
    name: 'school.member.students.index',
    path: '',
    component: StudentIndex,
    meta: {
        breadcrumb: 'index',
        title: 'Students',
    },
};
