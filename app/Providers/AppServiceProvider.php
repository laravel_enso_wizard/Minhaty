<?php

namespace App\Providers;
use App\Http\Requests\People\ValidatePersonStore;
use App\Http\Requests\People\ValidatePersonUpdate;
use Illuminate\Support\ServiceProvider;
use LaravelEnso\People\app\Http\Requests\ValidatePersonStore as EnsoPersonStoreValidator;
use LaravelEnso\People\app\Http\Requests\ValidatePersonUpdate as EnsoPersonUpdateValidator;

class AppServiceProvider extends ServiceProvider
{

    public $bindings = [
        EnsoPersonStoreValidator::class   => ValidatePersonStore::class,
        EnsoPersonUpdateValidator::class  => ValidatePersonUpdate::class,
    ];
    
    public function boot()
    {
        //
    }

    public function register()
    {
        //
    }
}
