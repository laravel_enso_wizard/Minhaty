<?php

namespace App\Tables\Builders\School\Member;

use App\Student;
use Illuminate\Database\Eloquent\Builder;
use LaravelEnso\Tables\app\Contracts\Table;

class StudentTable implements Table
{
    protected const TemplatePath = __DIR__.'/../../../Templates/School/Member/students.json';

    public function query(): Builder
    {
        return Student::selectRaw('
            students.id as "dtRowId",
            students.fname,
            students.lname,
            students.cne,
            students.email,
            students.naissance,
            students.cni,
            students.city,
            students.yatim
        ');
        
    }

    public function templatePath(): string
    {
        return static::TemplatePath;
    }
}
