<?php

namespace App\Http\Requests\School\Member;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class ValidateStudentRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'fname'        => 'required|max:255',
            'lname' => 'required|max:255',
            'cne'        => 'required|max:255',
            'email' => 'required|max:255',
            'naissance'=>'required',
            'cni'=>'required|max:255',
            'city'=>'required|max:255',
            'yatim'=>'required|max:255'
        ];
    }
}
