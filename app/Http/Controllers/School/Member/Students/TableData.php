<?php

namespace App\Http\Controllers\School\Member\Students;

use App\Tables\Builders\School\Member\StudentTable;
use Illuminate\Routing\Controller;
use LaravelEnso\Tables\app\Traits\Data;

class TableData extends Controller
{
    use Data;

    protected $tableClass = StudentTable::class;
}
