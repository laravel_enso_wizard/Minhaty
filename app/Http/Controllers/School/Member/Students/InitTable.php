<?php

namespace App\Http\Controllers\School\Member\Students;

use App\Tables\Builders\School\Member\StudentTable;
use Illuminate\Routing\Controller;
use LaravelEnso\Tables\app\Traits\Init;

class InitTable extends Controller
{
    use Init;

    protected $tableClass = StudentTable::class;
}
