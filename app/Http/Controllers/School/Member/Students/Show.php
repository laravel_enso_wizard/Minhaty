<?php

namespace App\Http\Controllers\School\Member\Students;

use App\Student;
use Illuminate\Routing\Controller;

class Show extends Controller
{
    public function __invoke(Student $student)
    {
        return ['student' => $student];
    }
}
