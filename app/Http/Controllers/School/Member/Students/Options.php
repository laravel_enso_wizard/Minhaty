<?php

namespace App\Http\Controllers\School\Member\Students;

use App\Student;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use LaravelEnso\Select\app\Traits\OptionsBuilder;

class Options extends Controller
{
    use OptionsBuilder;

    protected $model = Student::class;

    //protected $queryAttributes = ['name'];

    //public function query(Request $request)
    //{
    //    return Student::query();
    //}
}
