<?php

namespace App\Http\Controllers\School\Member\Students;

use App\Tables\Builders\School\Member\StudentTable;
use Illuminate\Routing\Controller;
use LaravelEnso\Tables\app\Traits\Excel;

class ExportExcel extends Controller
{
    use Excel;

    protected $tableClass = StudentTable::class;
}
