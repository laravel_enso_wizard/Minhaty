<?php

namespace App\Http\Controllers\School\Member\Students;

use Illuminate\Routing\Controller;
use App\Forms\Builders\School\Member\StudentForm;

class Create extends Controller
{
    public function __invoke(StudentForm $form)
    {
       // dd($form);
        return ['form' => $form->create()];
    }
}
