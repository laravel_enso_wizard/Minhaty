<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Routing\Controller;
use LaravelEnso\Examples\Tables\Builders\ExampleTable;
use LaravelEnso\Tables\app\Traits\Init;
use app\Tables\Builders\School\Member\StudentTable;
use LaravelEnso\Permissions\app\Tables\Builders\PermissionTable;

class TableInit extends Controller
{
    use Init;
    protected $tableClass = PermissionTable::class;
}
