<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Routing\Controller;
use LaravelEnso\Examples\Tables\Builders\ExampleTable;
use LaravelEnso\Tables\app\Traits\Data;
use LaravelEnso\Permissions\app\Tables\Builders\PermissionTable;
class TableData extends Controller
{
    use Data;
    protected $tableClass = PermissionTable::class;
}
